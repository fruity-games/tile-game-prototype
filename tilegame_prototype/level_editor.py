"""!
Basic level editor for creating, viewing and testing levels.
This is the main application of the prototype.
"""

# Imports
from random import randint
import json
# Managers
from tilegame_prototype.tiles import TileManager
from tilegame_prototype.gameplay.game_manager import GameManager
from tilegame_prototype.gui.gui_manager import GUIManager
# Misc
from tilegame_prototype.game_utils.constants import EditorModes, TileEdges
from tilegame_prototype.game_utils.common import Loc


# LevelEditor (manager)

class LevelEditor(object):
    """!
    Create and manage a level editor with GUI and level loading
    """

    ## Manager of GUI
    gui_mn = None
    ## Manager of tiles
    tile_mn = None
    ## Manager of gameplay
    game_mn = None

    # Level play setup
    enemies_setup = [] # Template of enemy objects for level

    # Level simulate play
    ## Whether player is allowed to move. SIM_PLAY mode only.
    can_pl_move = False

    # Level editor usage
    ## Current mode editor is in
    editor_mode = EditorModes.VIEW
    ## Name of currently loaded level
    level_name = "DefaultLevel"
    ## File name of level currently operating on
    cur_save_name = ""

    def __init__(self, master=None):
        """!
        Initialise level managers and prepare for usage

        @param master: (widget) widget to create the LevelEditor GUI in. Should
            be a Toplevel widget. If None, GUI will not be created
        """
        # Initialise managers
        self.tile_mn = TileManager() # Used in all modes
        self.game_mn = GameManager(self) # Used in SIM_PLAY mode only
        if master:
            self.gui_mn = GUIManager(master, self) # Create GUI if necessary

    def clear_current(self):
        """!
        Clear all current elements in the level setup and restore its
        state to the initial state
        """
        # Initialise managers
        self.tile_mn.reset()
        self.game_mn.reset()
        if self.gui_mn:
            self.gui_mn.reset()

        # Reset self
        self.reset()

        self.update_gui()

    def reset(self):
        """!
        Reset values of level editor. Use clear_current for full reset
        """
        # Reset own values
        self.editor_mode = EditorModes.VIEW
        self.level_name = "DefaultLevel"
        self.enemies_setup = []
        self.can_pl_move = False
        self.cur_save_name = ""

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # File Operations                                                     #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def save_to_file(self):
        """!
        Save the current level to file using the current level name as
        the filename, overwriting existing level with same name if possible.

        @return: (bool) Whether file was saved successfully
        """
        # Filename based on level name
        return self._save_to_file("%s.json" % self.level_name)

    def load_from_file(self, filename):
        """!
        Load a level from JSON file. Current unsaved level will be discarded.

        @param filename: (str) Filename to load level from. Should be
            as "*.json" to load the JSON file.

        @return: (bool) Whether level was loaded successfully
        """

        # Load level from file

        try:
            with open(filename) as file:
                # Decode JSON into a dictionary
                loaded_lvl = json.load(file)
        except IOError:
            return False # Failure


        # Clear current values
        self.clear_current()

        # Set current file name for quick saving in future
        self.cur_save_name = filename


        # Set level editor values from loaded level

        tiles = loaded_lvl["Tiles"]
        self.tile_mn.set_tiles_from_load(tiles)

        enemies = loaded_lvl["Enemies"]
        self.set_enemies_from_load(enemies)

        gameplay = loaded_lvl["Gameplay"]
        self.game_mn.set_gameplay_from_load(gameplay)

        level_metadata = loaded_lvl["Level Metadata"]
        self.set_level_metadata_from_load(level_metadata)

    def set_enemies_from_load(self, enemies_dict):
        """!
        Add enemies to list loaded from JSON file
        """
        for enemy_dict in enemies_dict:
            self.add_enemy_from_load(enemy_dict)

    def add_enemy_from_load(self, enemy_dict):
        """!
        Add single enemy to list as loaded from JSON file
        """
        loc = enemy_dict["location"]
        # Parse origin_offset as Loc of integers
        loc = Loc(*(int(x) for x in loc.strip("()").split(",")))

        # Create and set attributes
        new_enem = self.game_mn.add_enemy_setup(loc)
        new_enem.friendly_name = enemy_dict["friendly_name"]
        for point in enemy_dict["route"]:
            new_enem.add_route_point(TileEdges[point])

    def set_level_metadata_from_load(self, level_metadata_dict):
        self.level_name = level_metadata_dict["level_name"]

    def _save_to_file(self, filename):
        """!
        Save the current level to file, overwriting existing level with
        same name if possible.

        @param filename: (str) name of file to write to

        @return: (bool) Whether file was saved successfully
        """

        try:
            tiles = \
            {
            "finish_tile":repr(self.tile_mn.finish_tile.origin_offset),

            "grid":
                {
                repr(tile.origin_offset):
                    {
                    "friendly_name": tile.friendly_name,
                    "connections"  : [e.name for e, t in tile.get_connected()]
                    }
                for origin_offset, tile in self.tile_mn.tile_grid.items()
                }
            }

            enemies = \
            [
                {
                "location":repr(enem.loc),
                "friendly_name":enem.friendly_name,
                "route":[point.name for point in enem.route]
                }
                for enem in self.enemies_setup
            ]

            level_metadata = \
            {
            "level_name":self.level_name

            # TODO: Save Extras to File (WIP)
            # Add additional Level Metadata such as desciption, difficulty,
            # rating, etc, to this section

            }

            gameplay = \
            {

            # TODO: Save Extras to File (WIP)
            # Add gameplay attributes like max_num_moves, min_num_moves, etc
            # to this section

            }

        except AttributeError:
            # One or more attributes weren't able to be retrieved
            return False # Failure

        to_write = {"Tiles" : tiles,
##                    "Tile Manager" : tile_manager,
                    "Enemies" : enemies,
                    "Level Metadata" : level_metadata,
                    "Gameplay":gameplay}

        # Write to JSON file
        try:
            with open(filename, "w") as file:
                json.dump(to_write, file, indent=4)
        except IOError:
            return False # Failure

        # Set current file name for quick saving in future
        self.cur_save_name = filename

        return True # Success

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # SIM_PLAY mode                                                       #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def restart_level(self):
        """!
        Restart current level (when in SIM_PLAY mode)
        """

        # Reset simulated level GameManager
        self.game_mn.reset_level()

        # Start turn loop
        self.game_mn.turnloop_start()

        self.update_gui()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Helper functions                                                    #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def update_gui(self):
        """!
        Tell GUI to refresh values to match state of the level
        """
        if self.gui_mn:
            self.gui_mn.update_gui()

    def set_pl_can_move(self, player, new_can_move):
        """!
        Set whether player can move and configure GUI to
        handle input.

        @param player: (Player) player to change
        @param new_can_move: (bool) whether player can move
        """
        self.can_pl_move = new_can_move
        self.update_gui()

    def wait_rand(self, callback=None):
        """!
        Wait a random amount of time, such as during enemy turns
        Only works if GUI exists

        If GUI doesn't exist, callback immediately
        """
        if self.gui_mn:
            # tkinter after function takes delay in ms
            self.gui_mn.main_gui.after(randint(300, 500), callback)
            return
        else:
            callback()