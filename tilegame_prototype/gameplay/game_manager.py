"""!
GameManager tracks state of current in-play game session
"""

# Imports
from tilegame_prototype.game_utils.constants import Team
from tilegame_prototype.gameplay.pawns import Enemy, Player
import gc

# GameManager class

class GameManager(object):
    """!
    Track state of current in-play game session, including
    win/lose conditions and player/enemy turns.
    """

    ## Whether turn loop can continue turning
    _turn_can_continue = False
    ## Active teams contaning players and enemies
    teams = {}
    ## Turn order of active teams
    _team_order = []

    def __init__(self, level_editor):
        """!
        Set initial values

        @param level_editor: (LevelEditor) level editor to be managed by
        """

        # Set level editor reference
        ## Reference to level editor
        self.level_editor = level_editor

        # Initial reset
        self.reset_level()

    def reset(self):
        """!
        Reset own values to initial position
        """
        self.reset_level()

        self._turn_can_continue = False
        self.teams = {}
        self._team_order = []

    def reset_level(self):
        """!
        Reset values for level, including any dynamic pawns
        """

        # Delete previous pawns before making new ones
        # This shouldn't be needed, but the old objects are kept for some reason
        try:
            # Delete old player
            a = gc.get_referrers(self.get_player())
            del a[0][-1]
        except:
            pass

        # Create and set 2 empty default teams
        team_pl = Team("Player")

        # Set references to teams
        self.teams = {"Player":team_pl}
        self._team_order = ["Player"] # Ensure order is maintained

        # Set team and player indexes
        self.cur_team_idx = 0 # Index of current team
        self.cur_pl_idx =   0 # Index of current player in team

        # Set turn and round counters
        self.num_turns =  0 # Number of turns played since level start
        self.num_rounds = 0 # Number of rounds since level start

        # Create player and add to player team
        self.add_player()

        # Create enemies using LevelEditor definition
        # Only create if any enemies added
        if len(self.level_editor.enemies_setup) > 0:
            team_en = Team("Enemy")
            self.teams["Enemy"] = team_en
            self._team_order.append("Enemy")
            for enemy in self.level_editor.enemies_setup:
                new_en = self.add_enemy(enemy.loc) # Add to enemy team
                new_en.route = enemy.route.copy() # Copy over defined route

    def set_gameplay_from_load(self, gameplay_dict):
        """!
        Set gameplay options loaded from JSON file.
        """
        # TODO: Set gameplay options here
        pass

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Turn based gameplay                                                 #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def turnloop_start(self):
        """!
        Start the player and enemy turn loop. Resume from same position as
        ended if possible.
        """
        self._turn_can_continue = True

        # Set to previous player value as next_turn will increment
        self.cur_pl_idx -= 1

        # Start the loop
        self.next_turn()


    def next_turn(self):
        """!
        End the current turn and start the next player or enemy turn
        """

        # Update GUI for end of turn
        self.level_editor.update_gui()

        # Increment level stats
        self.num_turns += 1

        # Increment player and team index to determine next player

        self.cur_pl_idx += 1 # Increment player index
        if self.cur_pl_idx >= len(self.get_cur_team().pawns):
            self.cur_pl_idx = 0 # Loop back to 0

            self.cur_team_idx += 1 # Increment team index
            if self.cur_team_idx >= len(self._team_order):
                self.cur_team_idx = 0 # Loop back to 0
                # Increment level stats
                self.num_rounds += 1

        # Start next turn if allowed
        if self._turn_can_continue:
            return self.get_cur_pl().on_start_turn()

    def turnloop_end(self):
        """!
        End the player and enemy turn loop after the current loop is over
        """
        self._turn_can_continue = False # will stop on next next_turn

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Player and team game setup                                          #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def add_player(self):
        """!
        Create new Player pawn and add to the Player team

        @return (Player) newly created player
        """
        # Create new object
        pl = Player(self) # Create at origin

        # Add to team
        self.teams["Player"].add_pawn(pl)

        return pl # Return new object

    def add_enemy(self, location):
        """!
        Create new Enemy pawn and add to the Enemy team

        @param location: (Loc) starting location of enemy

        @return (Enemy) newly created enemy
        """
        # Create new object
        en = Enemy(self, location)

        # Add to team
        self.teams["Enemy"].add_pawn(en)

        return en # Return new object

    def add_enemy_setup(self, location):
        """!
        Create new Enemy pawn and add to level setup

        @return: (Enemy) newly created enemy
        """
        # Create new object
        en = Enemy(None, location)

        # Add to setup
        self.level_editor.enemies_setup.append(en)

        return en # Return new object

    def move_player(self, edge):
        # Tell player to move
        self.get_player().add_movement(edge)

        # Refresh editor gui
        self.level_editor.update_gui()

        # End player's turn
        self.get_player().end_turn()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Helper functions                                                    #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def get_cur_pl(self):
        """!
        Returns player that has their turn now

        @return (Pawn) current player
        """
        return self.get_cur_team().pawns[self.cur_pl_idx]

    def get_cur_team(self):
        """!
        Returns team that has their turn now

        @return (Team) current team
        """
        return self.teams[self._team_order[self.cur_team_idx]]

    def get_player(self, id=0):
        """!
        Returns human Player at id

        @param id: (int) index of player to get. Can be left to 0

        @return (Player) player object
        """
        return self.teams["Player"].pawns[id]

    def get_pawns_by_loc(self):
        """!
        Returns all pawns grouped by who is on the same location

        @return (dict) location : pawns_at_location
        """

        # Dictionary of tiles
        pawns = {} # tile : pawns_on_tile

        # Iterate through all pawns
        for team in self.teams:
            for pawn in self.teams[team].pawns:
                try:
                    pawns[tuple(pawn.loc)].append(pawn)
                except KeyError:
                    pawns[tuple(pawn.loc)] = [pawn]

        # Return found locations
        return pawns
