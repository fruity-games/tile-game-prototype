"""!
Contains Pawn base class, as well as Player and Enemy
child classes to move around the level during play.

Pawns should be created through GameManager
"""

# Imports
from tilegame_prototype.game_utils.common import Loc
from tilegame_prototype.game_utils.constants import TileEdges


# Base Pawn class

class Pawn(object):
    """!
    Base class for moving entities in the level that
    can be on a tile.
    """

    ## GameManager this pawn is tracked by
    game_mn = None
    ## Display name for this pawn
    friendly_name = "Pawn"

    def __init__(self, game_mn, loc=None):
        """!
        Initialise pawn to be manager by a game manager

        @param game_mn: (GameManager) game manager to be tracked by
        @param loc: (Loc) initial location to create pawn at.
            If None, will be set to origin.
        """
        # Set manager reference
        self.game_mn = game_mn

        # Set initial location
        if loc != None:
            self.loc = Loc(*loc) # Specified loc
            # Ensure it is always a Loc object
        else:
            self.loc = Loc(0, 0) # Origin

    def on_start_turn(self):
        """!
        Called by GameManager to start turn (move somewhere)
        """
        # Must call end_turn so that turn loop continues
        self.end_turn()

    def end_turn(self):
        """!
        End this pawn's turn
        """
        if self.game_mn:
            self.game_mn.next_turn()

    def add_movement(self, edge):
        """!
        Move pawn to tile connected at edge

        @param edge: (TileEdges enum) edge of current tile to move to
        """

        cur_tile = self.game_mn.level_editor.tile_mn.get_tile_at(self.loc)

        # Find tile to move to
        other_tile = None
        for side, tile in self.get_move_targets():
            if side == edge:
                other_tile = tile

        # Check we are moving to a valid tile
        if other_tile:
            # Set pawn location
            self.loc = other_tile.origin_offset

    def get_move_targets(self):
        """!
        Yields which edges have a tile that we can move to

        yield: (TileEdges, Tile) active tile connection
        """
        return self.game_mn.level_editor.tile_mn.get_tile_at(self.loc).get_connected()


# Children of Pawn class for actual use in-game

class Player(Pawn):
    """!
    Pawn with user controlled movement input
    """

    friendly_name = "Player"

    def on_start_turn(self):

        # Notify level editor we can move
        self.game_mn.level_editor.set_pl_can_move(self, True)

    def end_turn(self):
        # Notify level editor to disable movement input
        self.game_mn.level_editor.set_pl_can_move(self, False)

        # End turn
        super().end_turn()

class Enemy(Pawn):
    """!
    Pawn with predetermined movement route
    """

    friendly_name = "Enemy"

    def __init__(self, game_mn, loc=None):

        # Set default values
        super().__init__(game_mn, loc)

        # Set route attributes
        self.route = [] # List of directions to take
        self.route_idx = -1 # Current stage on route, before first increment
        self.route_dir = 1  # Current direction of route (1 or -1)

    def add_route_point(self, edge_dir, num_repeats=1):
        """!
        Add a movement direction to the predetermined enemy route.
        Route should not be modified while the game is playing.
        Will not check if this is a valid move until runtime.

        @param edge_dir: (TileEdges edge) direction to move in
        @param num_repeats: (int) number of times to move in the given direction
        """
        for i in range(num_repeats):
            self.route.append(edge_dir)

    def remove_route_point(self, idx=None):
        """!
        Remove route point at idx, or pop last added if left as None
        """
        if idx == None:
            self.route.pop() # Pop last index
            return

        try:
            # Check idx is valid
            self.route.pop(idx)
            return
        except IndexError:
            return

    def on_start_turn(self):
        """!
        Wait random delay before making move along route
        """

        # Wait random amount of time then do move
        self.game_mn.level_editor.wait_rand(self._route_step)

    def _route_step(self):
        """!
        Move forward one step in the route
        """

        # End turn immediately if given no route
        if len(self.route) == 0:
            return self.end_turn()

        # Step forward in route index
        self.route_idx += self.route_dir

        # Loop steps back around if necessary
        if self.route_idx < 0:
            self.route_idx = 0  # Go to 2nd tile
            self.route_dir = 1  # Switch to move forward
        elif self.route_idx >= len(self.route):
            self.route_idx = len(self.route) - 1  # Go to 2nd to last tile
            self.route_dir = -1 # Switch to move backward

        # Determine direction to move in
        mv_direction = self.route[self.route_idx]

        # Reverse direction if going backward
        if self.route_dir == -1:
            if mv_direction == TileEdges.TOP:
                mv_direction = TileEdges.BOTTOM
            elif mv_direction == TileEdges.RIGHT:
                mv_direction = TileEdges.LEFT
            elif mv_direction == TileEdges.BOTTOM:
                mv_direction = TileEdges.TOP
            elif mv_direction == TileEdges.LEFT:
                mv_direction = TileEdges.RIGHT

        # Perform desired move
        self.add_movement(mv_direction)

        # End turn
        self.end_turn()


