"""!
Contains high level level editor GUI structure and
interaction with application
"""

# Imports

from tkinter import filedialog, messagebox
from tilegame_prototype.gui.submenus import LevelEditor_GUI, AppMenuBar
from tilegame_prototype.game_utils.constants import TileEdges, EditorModes
from tilegame_prototype.game_utils.common import Common
from tilegame_prototype.gameplay.pawns import Enemy


# GUIManager class

class GUIManager(object):
    """!
    Manager for all GUI elements in the level editor application and
    GUI interaction with application.
    """

    # Tile colors
    ## Color of tiles by default
    tile_col_default = "darkgrey"
    ## Color of origin tile
    tile_col_origin = "green"
    ## Color of finish tile
    tile_col_finish = "red"

    ## Reference to LevelEditor object
    level_editor = None

    ## Index of selected enemy, or None if not selected
    selected_enemy_idx = None

    def __init__(self, master, level_editor):
        """!
        Create initial level editor menu
        """

        ## Persistent GUI values to reuse between updates
        self.cached = {}

        # Set level editor reference
        self.level_editor = level_editor

        # Create application menu bar
        menu_bar = AppMenuBar(master, g=self)
        try:
            master.config(menu=menu_bar)
        except:
            menu_bar.destroy()

        # Create main GUI widget
        self.main_gui = LevelEditor_GUI(master)
        self.main_gui.pack()

        # Set application root hotkeys
        master.bind("<Control-o>", lambda e: self.file_open())
        master.bind("<Control-s>", lambda e: self.file_save())
        master.bind("<Control-Shift-S>", lambda e: self.file_save_as())

        # Update GUI with initial values
        self.update_gui()

    def reset(self):
        """!
        Reset own values to initial position
        """

        self.cached = {}
        self.selected_enemy_idx = None

        self.update_gui()

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # GUI interaction - called from GUI                                   #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def set_editor_mode(self, new_mode):
        """!
        Set editor usage mode

        @param new_mode: (EditorModes mode) new usage mode
        """
         # Only allow a valid enum mode
        if not new_mode in EditorModes \
           or new_mode == self.editor_mode: # Mode hasn't changed
            return

        self.level_editor.editor_mode = new_mode

        # Update any data if necessary
        self.on_mode_changed()

        self.update_gui() # update GUI when changing properties

    def set_level_name(self, new_name):
        """!
        Set editor usage mode

        @param new_name: (str) new name for level
        """
        # Only allow non empty strings
        if new_name:
            self.level_editor.level_name = new_name

        self.update_gui()

    def add_tile_from(self, other_tile, new_edge):
        """!
        Add a tile from GUI when in Edit mode

        @param other_tile: (Tile) EXISTING tile to connect to
        @param new_edge: (TileEdges edge) edge of NEW tile to connect with
            existing tile
        """
        # Swap new_edge to get other_edge
        other_edge = Common.get_reverse_edge(new_edge)

        # Create the new tile
        self.level_editor.tile_mn.add_tile(other_tile, other_edge)

        # Refresh GUI
        self.update_gui()

    def set_end_tile(self, tile):
        """!
        Set tile as the end tile

        @param tile: (Tile) tile to set as the end tile
        """
        self.level_editor.tile_mn.set_finish_tile(tile)
        self.update_gui()

    def add_enemy(self, tile):
        """!
        Add an enemy with starting location of tile TO THE LEVEL SETUP,
        not to the in-play level.
        """
        en = Enemy(None, tile.origin_offset)
        # Give enemy a new name
        en.friendly_name = "Enemy %s" \
            % Common.letter_code_by_idx(len(self.level_editor.enemies_setup))

        # Add to setup list
        self.level_editor.enemies_setup.append(en)
        self.update_gui()

    def remove_tile(self, tile):
        """!
        Remove chosen tile from grid

        @param tile: (Tile) tile to remove from grid
        """
        self.level_editor.tile_mn.remove_tile(tile)
        self.update_gui()

    def select_enemy(self, idx):
        """!
        Select level setup enemy at index idx
        """
        if idx != self.selected_enemy_idx:
            self.selected_enemy_idx = idx
            self.update_gui()

    def deselect_enemy(self):
        """!
        Deselect any selected enemy
        """
        self.selected_enemy_idx = None
        self.update_gui()

    def remove_selected_enemy(self):
        """!
        Remove currently selected enemy
        """
        # Remove from level
        self.level_editor.enemies_setup.pop(self.selected_enemy_idx)

        # Clear selection
        self.selected_enemy_idx = None

        self.update_gui()

    def add_route_point(self, direction):
        """!
        Add a point to currently selected enemy's route

        @param value (str) one of "^>v<" denoting direction
        """

        # Confirm editing enemy is valid
        enemy_to_edit = self.get_selected_enemy()
        if not enemy_to_edit:
            return

        if direction == "^":
            enemy_to_edit.add_route_point(TileEdges.TOP)
        elif direction == ">":
            enemy_to_edit.add_route_point(TileEdges.RIGHT)
        elif direction == "v":
            enemy_to_edit.add_route_point(TileEdges.BOTTOM)
        elif direction == "<":
            enemy_to_edit.add_route_point(TileEdges.LEFT)

        self.update_gui()

    def remove_route_point(self):
        """!
        Remove selected enemy's selected route point
        """

        # Get selected route
        idx = self.cache_get("en_route_list_sel_idx")

        # Do nothing
        if idx == None:
            return

        # Confirm editing enemy is valid
        enemy_to_edit = self.get_selected_enemy()
        if not enemy_to_edit:
            return

        enemy_to_edit.remove_route_point(idx)

        # Remove selection
        self.cache_set("en_route_list_sel_idx", None)

        self.update_gui()

    def file_new(self):
        if messagebox.askokcancel("New Level", "Are you sure you want to "
            "clear the currently loaded level and make a new level?\n\nAny "
            "unsaved progress will be lost."):
            self.level_editor.clear_current()

        self.update_gui()

    def file_open(self):
        filename = filedialog.askopenfilename(title="Select Level",
            filetypes = (("Level Files","*.json"),("Level Files","*.JSON")))
        if filename:
            self.level_editor.load_from_file(filename)

        self.update_gui()

    def file_save(self):
        # Use currently loaded level file if possible
        if self.level_editor.cur_save_name:
            self.save_to_file(self.level_editor.cur_save_name)
        # Otherwise filename must be chosen
        else:
            self.file_save_as()

    def file_save_as(self):
        filename = filedialog.asksaveasfilename(title="Select Level",
            filetypes = (("Level Files","*.json"),("Level Files","*.JSON")))
        if filename:
            # Add file extension if not already added
            if not (filename.endswith(".json") or filename.endswith(".JSON")):
                filename += ".json"

            self.save_to_file(filename)

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # Helper functions                                                    #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

    def update_gui(self):
        """!
        Tell GUI to refresh values to match state of the level
        """
        if self.main_gui:
            # Match state of level editor this manager is managed by
            self.main_gui.__refresh__(self)

    def on_mode_changed(self):
        """!
        Called when editor mode changes
        """

        if self.level_editor.editor_mode == EditorModes.SIM_PLAY:
            self.level_editor.restart_level()

        if self.level_editor.editor_mode != EditorModes.EDIT:
            self.selected_enemy_idx = None

    def get_setup_enemies_by_loc(self):
        """!
        Returns all level setup enemies grouped by who is on the same location

        @return (dict) location : pawns_at_location
        """

        # Dictionary of tiles
        enemies = {} # tile : enemies_on_tile

        # Iterate through all enemies
        for enemy in self.level_editor.enemies_setup:
            try:
                # Add to existing list
                enemies[tuple(enemy.loc)].append(enemy)
            except KeyError:
                # Create new list with enemy
                enemies[tuple(enemy.loc)] = [enemy]

        # Return found locations
        return enemies

    def restart_level(self):
        """!
        Restart the in-play level
        """
        self.level_editor.restart_level()
        self.update_gui()

    def cache_set(self, key, value):
        """!
        Add a value to cache. Updates existing value if possible
        """
        self.cached[key] = value

    def cache_get(self, key, default=None):
        """!
        Get a value from cache. Returns default if not found
        """
        return self.cached.get(key, default)

    def get_selected_enemy(self):
        """!
        Return Enemy object who is currently selected, or None if
        no enemy is selected.
        """
        # Check that an enemy is selected
        if self.selected_enemy_idx == None:
            return None

        return self.level_editor.enemies_setup[self.selected_enemy_idx]

    def is_origin(self, tile):
        """!
        Return whether tile is the origin tile
        """
        return self.level_editor.tile_mn.is_origin(tile)

    def is_finish(self, tile):
        """!
        Return whether tile is the finish tile
        """
        return self.level_editor.tile_mn.is_finish(tile)

    def get_full_tile_name(self, tile):
        """!
        Return full name of tile
        """
        return self.level_editor.tile_mn.get_full_name(tile)

    def save_to_file(self, filename):
        """!
        Save the level to filename and show GUI message afterwards of either
        a confirmation or an error.
        """
        # Perform the save
        was_saved = self.level_editor._save_to_file(filename)

        # Show confirmation or error message
        title = "Save Level"
        if was_saved:
            messagebox.showinfo(title, "Level saved successfully!")
        else:
            msg = "Level could not be saved.\n\n"
            # Perform error diagnostics for intelligent error message
            # Common mistakes
            if not self.level_editor.tile_mn.finish_tile:
                msg += "No tile has been set as the end tile "
                "(Edit Mode, Right Click on tile->Make End).\n"
            # Show message
            messagebox.showerror(title, msg)


    # Property accessors for LevelEditor properties

    @property
    def editor_mode(self):
        return self.level_editor.editor_mode

    @property
    def level_name(self):
        return self.level_editor.level_name

    @property
    def tile_mn(self):
        return self.level_editor.tile_mn

    @property
    def game_mn(self):
        return self.level_editor.game_mn

    @property
    def enemies_setup(self):
        return self.level_editor.enemies_setup

    @property
    def can_pl_move(self):
        return self.level_editor.can_pl_move

