"""!
Submenus for level editor
"""

# Imports
from tkinter import * # for base GUI
from tkinter.ttk import * # modernise common widgets
from tilegame_prototype.game_utils.common import Loc, MathStat
from tilegame_prototype.game_utils.gui import SubmenuBase, PopupBase, EditableText
from tilegame_prototype.game_utils.constants import EditorModes, TileEdges
from tilegame_prototype.gameplay.pawns import Player

# Popup templates for level editor
class Popup_TileEdit(PopupBase):
    def __preconfig__(self, kw):
        super().__preconfig__(kw)

        ## Reference to tile this popup is editing
        self.tile = kw.pop("tile",
                    kw.pop("t", None))

    def __make__(self, cnf):
        # Tile and LevelEditor must be specified for commands to work
        if not self.tile or not self.gui_mn:
            return

        # Add popup items
        self.add_command(label="Tile %s" % self.gui_mn.get_full_tile_name(self.tile))

        # Don't create any tile options for origin tile
        if self.gui_mn.is_origin(self.tile):
            return

        # Add tile commands
        self.add_separator()

        # Make end tile if not already end tile
        if not self.gui_mn.is_finish(self.tile):
            self.add_command(label="Make End",
                command=lambda: self.gui_mn.set_end_tile(self.tile))

        # Add enemy here
        self.add_command(label="Add Enemy Here",
            command=lambda: self.gui_mn.add_enemy(self.tile))

        # Remove tile
        self.add_command(label="Delete",
            command=lambda: self.gui_mn.remove_tile(self.tile))


# Submenu templates for level editor

class Templ_TitleBanner(SubmenuBase):
    """!
    Top banner for showing key level info
    """
    def __make__(self, cnf):
        # Create level name Label
        self.level_name_frame = Frame(self)
        self.level_name_frame.pack(side=LEFT, padx=10)


        # Create level editor GUI refresh button
        self.refresh_btn = Button(self,
            text="Refresh GUI",
            command=None # set command when given level editor on refresh
            )
        self.refresh_btn.pack(side=RIGHT, padx=10)

    def __refresh__(self, gui_mn):

        # Destroy old level name widget
        for w in self.level_name_frame.winfo_children():
            w.destroy()

        # Create level name widget
        self.level_name_var = StringVar()

        # Create editable text for edit mode
        if gui_mn.editor_mode == EditorModes.EDIT:
            EditableText(self.level_name_frame,
                textvariable=self.level_name_var,
                command=gui_mn.set_level_name, # Input is passed through
                hint="Enter level name..."
                ).pack()

        # Otherwise create view only text
        else:
            Label(self.level_name_frame,
                textvariable=self.level_name_var
                ).pack()

        # Set level name
        self.level_name_var.set(gui_mn.level_name)

        # Set refresh command
        self.refresh_btn.config(command=gui_mn.update_gui)

class Templ_EditorPropPanel(SubmenuBase):
    """!
    Panel for adjusting properties of the level editor
    """
    gui_mn = None

    def __make__(self, cnf):
        # Create level name Label
        Label(self,
            text="Level Editor Properties"
            ).pack(padx=3, pady=10)


        # Editor mode customisation widgets

        # Create frame with label
        mode_frame = LabelFrame(self, text="Editor Mode")
        mode_frame.pack(fill=X, padx=3, pady=3)

        # Create options menu widget
        self.mode_var = StringVar() # string holder for current data
        modes = [m.name.title() for m in EditorModes]
        OptionMenu(mode_frame, self.mode_var, modes[0], *modes,
                   command=self.on_mode_changed
                   ).pack(fill=X, expand=TRUE, padx=3, pady=3)

    def __refresh__(self, gui_mn):
        # Set object reference for widget callbacks
        self.gui_mn = gui_mn

        # Set editor mode
        self.mode_var.set(gui_mn.editor_mode.name.title())

    def on_mode_changed(self, new_mode):
        if self.gui_mn:
            self.gui_mn.set_editor_mode(EditorModes[new_mode.upper()])

class Templ_Viewport(SubmenuBase):
    """!
    Viewport for viewing the loaded level
    """

    max_grid_bound = 11 # Maximum number of tiles to draw
    central_loc = Loc(5, 5) # Location of center (origin) tile

    def __make__(self, cnf):
        self.canvas = Canvas(self)
        self.canvas.pack(fill=BOTH, expand=TRUE)

    def __refresh__(self, gui_mn):

        # Remove all previous drawings
        self.canvas.delete(ALL)

        # Get current canvas size
        c_width = int(self.canvas.cget("width"))
        c_height = int(self.canvas.cget("height"))
        min_c_dim = min(c_height, c_width) # smallest canvas dimension

        # Get tile width to draw (actual canvas pixels)
        self.tile_sz = (min_c_dim // self.max_grid_bound) # in canvas pixels
        # Redraw tiles
        self.refresh_tiles(gui_mn)

        # Draw Pawns
        # Only draw for Simulate Play or Edit mode
        if gui_mn.editor_mode != EditorModes.VIEW:
            self.refresh_pawns(gui_mn)

    def refresh_tiles(self, gui_mn):
        """!
        Called on refresh to redraw tiles
        """

        # Clear old tiles
        self.canvas.delete("tile")

        # Create new tiles

        for i in range(self.max_grid_bound):
            for j in range(self.max_grid_bound):
                # Get tile from grid
                tile_loc = Loc(i, j) - self.central_loc
                tile_to_draw = gui_mn.tile_mn.get_tile_at(tile_loc)

                # Check if there is any tile here
                if tile_to_draw:
                    # Set location on static sized canvas pseudo-grid
                    loc1 = self.get_canvas_loc(tile_loc)
                    loc2 = loc1 + (self.tile_sz, - self.tile_sz)

                    # Set fill color
                    if tile_to_draw == gui_mn.tile_mn.origin_tile:
                        fill = gui_mn.tile_col_origin
                    elif tile_to_draw == gui_mn.tile_mn.finish_tile:
                        fill = gui_mn.tile_col_finish
                    else:
                        fill = gui_mn.tile_col_default

                    # Create the rectangle on the canvas
                    tile = self.canvas.create_rectangle(loc1, loc2,
                            fill=fill, outline=fill,
                            tags=("tile", repr(tile_loc)))

                    # Bind popup menu for tile
                    # Only bind for Edit mode
                    if gui_mn.editor_mode == EditorModes.EDIT:
                        self.canvas.tag_bind(repr(tile_loc), "<ButtonRelease-3>",
                            lambda e, g=gui_mn, t=tile_to_draw:
                            Popup_TileEdit(self.canvas, g=g, t=t).post(e.x_root, e.y_root))


                # Otherwise create Add Tile button
                # Only create for Edit mode
                elif gui_mn.editor_mode == EditorModes.EDIT:
                    adj_tile = None # First adjacent tile to this location
                    for edge in TileEdges:
                        # Check if tile exists at this edge
                        adj_loc = tile_loc + edge.value
                        adj_tile = gui_mn.tile_mn.get_tile_at(adj_loc)

                        # Break if there is an existing tile at this edge
                        if adj_tile:
                            break

                    # Check if an adjacent tile was found
                    if adj_tile:
                        # Set location on static sized canvas pseudo-grid
                        loc1 = self.get_canvas_loc(tile_loc)
                        loc2 = loc1 + (self.tile_sz, - self.tile_sz)
                        btn_loc = MathStat.lerp(loc1, loc2, .5)

                        # Create button on the canvas as a text
                        btn = self.canvas.create_text(btn_loc, text="+",
                            font="Arial 14",
                            tags=("tile", "tile_add", repr(tile_loc)))

                        # Bind button to add tile
                        self.canvas.tag_bind(btn, "<ButtonRelease-1>",
                            lambda x, t=adj_tile, e=edge:
                                gui_mn.add_tile_from(t, e))

    def refresh_pawns(self, gui_mn):
        """!
        Called on refresh to redraw pawns
        """

        # Clear old pawns
        self.canvas.delete("pawn")

        # Radius of circles
        rad = self.tile_sz * 0.25
        # Size of pawn circle
        pawn_sz = (self.tile_sz * 0.15, self.tile_sz * -0.15)

        # Decide which pawn list to use
        if gui_mn.editor_mode == EditorModes.SIM_PLAY:
            # Show player and use current enemy state
            pawns_by_loc = gui_mn.game_mn.get_pawns_by_loc()
        elif gui_mn.editor_mode == EditorModes.EDIT:
            # Don't show player and use enemy setup
            pawns_by_loc = gui_mn.get_setup_enemies_by_loc()

        for tile_loc in pawns_by_loc:

            tot_n = len(pawns_by_loc[tile_loc]) # Number of pawns on this tile


            # Iterate through all pawns on this location
            for i, pawn in enumerate(pawns_by_loc[tile_loc]):

                # Set location for circle
                tile_center = self.get_canvas_loc(tile_loc) + (self.tile_sz * 0.5, self.tile_sz * -0.5)

                if tot_n == 1:
                    center = tile_center
                else:
                    center = Loc(*MathStat.getpos_circle(i, tot_n, rad, tile_center))

                loc1 = center - pawn_sz
                loc2 = center + pawn_sz

                # Set fill color
                # Orange for player, blue for enemy
                fill = "orange" if isinstance(pawn, Player) else "blue"

                self.canvas.create_oval(loc1, loc2,
                    fill=fill, outline=fill, tags=("pawn", repr(tile_loc)))

    def get_canvas_loc(self, tile_loc):
        """!
        Return location of bottom left of tile in canvas coordinates

        @param tile_loc: (Loc) origin_offset of tile

        @return (Loc) canvas coordinates of tile's bottom left corner
        """

        # Get actual location of tile
        # Add Tile.origin_offset to central tile location
        canv_loc = (self.central_loc + tile_loc) * self.tile_sz

        # Invert y axis for tkinter canvas
        c_height = int(self.canvas.cget("height"))
        canv_loc.y = c_height - canv_loc.y

        # Return the canvas coordinates
        return canv_loc

class Templ_ViewPanel(SubmenuBase):
    """!
    Panel next to viewport when in VIEW mode
    """

    def __make__(self, cnf):
        # Create panel name Label
        Label(self,
            text="View Mode"
            ).pack(padx=3, pady=10)

    def __refresh__(self, gui_mn):
        pass

class Templ_EnemyList(SubmenuBase):
    """!
    Interactive list of enemy in the current level's enemy setup
    """
    def __make__(self, cnf):
        # Create enemy list label
        en_list_frame = LabelFrame(self, text="Enemy List")
        en_list_frame.pack()

        # Create list of currently added enemies
        en_listbox_frame = Frame(en_list_frame)
        en_listbox_frame.pack(padx=3, pady=3)

        en_list_scroll = Scrollbar(en_listbox_frame, orient=VERTICAL)
        en_list_scroll.pack(side=RIGHT, fill=Y)

        self.en_list = Listbox(en_listbox_frame, height=4, selectmode=SINGLE,
            exportselection=0)
        self.en_list.pack(side=LEFT, fill=BOTH, expand=TRUE)
        # Configure scrollbar function

        self.en_list.config(yscrollcommand=en_list_scroll.set)
        en_list_scroll.config(command=self.en_list.yview)

    def __refresh__(self, gui_mn):
        # Clear enemy list
        self.en_list.delete(0, END)

        # Add enemies to list
        for enemy in gui_mn.enemies_setup:
            self.en_list.insert(END, enemy.friendly_name)

        # Select enemy if selection was cached
        if gui_mn.selected_enemy_idx != None:
            self.en_list.selection_set(gui_mn.selected_enemy_idx)
        # Get cached scroll position
        self.en_list.yview_moveto(gui_mn.cache_get("en_list_yview", 0.0))

        # Binding to make enemy edit widgets
        self.en_list.bind("<Double-Button-1>", lambda e, g=gui_mn:
            self.after(0, lambda g=g, e=e: self.on_press_en(g, e)))

    def on_press_en(self, gui_mn, event):
        """!
        Called when an enemy in listbox is pressed
        """
        # Get single enemy who is currently selected
        sel_idx = self.en_list.curselection()
        if len(sel_idx) == 1:
            sel_idx = sel_idx[0]
        else:
            sel_idx = None

        # Save selection
        gui_mn.select_enemy(sel_idx)

        # Cache listbox scroll position
        try:
            gui_mn.cache_set("en_list_yview", self.en_list.yview()[1])
        except TclError:
            pass

class Templ_EnemyRoute(SubmenuBase):
    """!
    Interactive list of enemy route of an enemy in the current
    level's enemy setup with encapsulated addition and removal
    of any step
    """
    add_route_func = None

    def __make__(self, cnf):
        # Create enemy list label
        en_list_frame = LabelFrame(self, text="Route")
        en_list_frame.pack()

        # Create frame for list of currently added enemies
        en_listbox_frame = Frame(en_list_frame)
        en_listbox_frame.pack(padx=3, pady=3)

        # Create scrollbar for easy access
        en_list_scroll = Scrollbar(en_listbox_frame, orient=VERTICAL)
        en_list_scroll.pack(side=RIGHT, fill=Y)
        # Create listbox for displaying route
        self.en_list = Listbox(en_listbox_frame, height=4, selectmode=SINGLE,
            exportselection=0)
        self.en_list.pack(side=LEFT, fill=BOTH, expand=TRUE)

        # Configure scrollbar function
        self.en_list.config(yscrollcommand=en_list_scroll.set)
        en_list_scroll.config(command=self.en_list.yview)


        # Create frame for addition and removal
        add_rem_frame = Frame(en_list_frame)
        add_rem_frame.pack(fill=X, anchor=W, padx=3)

        # Create direction button
        self.dir_var = StringVar()
        self.dir_menu = OptionMenu(add_rem_frame, self.dir_var, "+",
                                   "^", ">", "v", "<",
                                   command=self.on_sel_dir)
        self.dir_menu.pack(side=LEFT)

        # Create remove button
        self.rem_btn = Button(add_rem_frame, text="-", width=3)
        self.rem_btn.pack(side=RIGHT, padx=3)

    def __refresh__(self, gui_mn):
        # Clear enemy list
        self.en_list.delete(0, END)

        # Get the Enemy object we are editing
        enemy_to_edit = gui_mn.get_selected_enemy()
        if enemy_to_edit == None:
            self.add_route_func = None
            self.rem_btn.config(command=None)
            return

        # Add directions to list
        for direction in enemy_to_edit.route:
            self.en_list.insert(END, direction.name.title()) # Enum in Title Case

        # Get cached selected direction
        sel_idx = gui_mn.cache_get("en_route_list_sel_idx")
        if sel_idx != None:
            self.en_list.selection_set(sel_idx)
        # Get cached scroll position
        self.en_list.yview_moveto(gui_mn.cache_get("en_route_list_yview", 0.0))

        # Binding to make enemy edit widgets
        self.en_list.bind("<Double-Button-1>", lambda e, g=gui_mn:
            self.after(0, lambda g=g: self.on_press_en(g)))


        # Set command to add route dir
        # This does not work. Must set command on initialisation
##        self.dir_menu.config(command=lambda v: gui_mn.add_route_point(v))
        # Workaround callback function ref
        self.add_route_func = gui_mn.add_route_point

        # Set command to remove route dir
        self.rem_btn.config(command=gui_mn.remove_route_point)

    def on_press_en(self, gui_mn):
        """!
        Called when an enemy in listbox is pressed
        """
        # Get single enemy who is currently selected
        sel_idx = self.en_list.curselection()
        if len(sel_idx) == 1:
            sel_idx = sel_idx[0]
        else:
            sel_idx = None

        # Cache selection
        gui_mn.cache_set("en_route_list_sel_idx", sel_idx)

        # Cache listbox scroll position
        try:
            gui_mn.cache_set("en_route_list_yview", self.en_list.yview()[1])
        except TclError:
            pass

    def on_sel_dir(self, direction):
        """!
        Workaround callback to add route path from selecting
        a value from the OptionMenu, since config doesn't allow
        setting its command
        """
        if self.add_route_func:
            self.add_route_func(direction)
            self.dir_var.set("+")

class Templ_EnemyEdit(SubmenuBase):
    """!
    Tools for editing an enemy for level setup
    """
    def __make__(self, cnf):

        # Create name label to show enemy who is getting edited
        self.name_labelframe = LabelFrame(self)
        self.name_labelframe.pack(fill=X, expand=TRUE, anchor=N)

        # Create Route editor

        self.route_edit = Templ_EnemyRoute(self.name_labelframe)
        self.route_edit.pack(fill=X, expand=TRUE)
        self.to_refresh.append(self.route_edit)


        # Create button to remove the enemy
        self.rm_btn = Button(self.name_labelframe, text="Delete")
        self.rm_btn.pack(side=LEFT, padx=3, pady=3)

        # Create button to end editiing the enemy
        self.ok_btn = Button(self.name_labelframe, text="OK")
        self.ok_btn.pack(side=LEFT, padx=3, pady=3)

    def __refresh__(self, gui_mn):
        # Ensure an enemy is selected
        if gui_mn.selected_enemy_idx == None:
            return

        # Get the Enemy object we are editing
        enemy_to_edit = gui_mn.get_selected_enemy()

        # Set enemy name
        self.name_labelframe.config(text="Editing %s" \
            % enemy_to_edit.friendly_name)

        # Set remove enemy button command
        self.rm_btn.config(command=gui_mn.remove_selected_enemy)

        # Set OK button command to end enemy edit
        self.ok_btn.config(command=gui_mn.deselect_enemy)

        # Refresh additionals
        super().__refresh__(gui_mn)


class Templ_EditPanel(SubmenuBase):
    """!
    Panel next to viewport when in EDIT mode
    """

    def __make__(self, cnf):
        # Create panel name Label
        Label(self,
            text="Edit Mode"
            ).pack(padx=3, pady=10)

        # Create enemy edit frame
        self.en_edit_frame = Frame(self)
        self.en_edit_frame.pack(fill=BOTH, expand=TRUE)

    def __refresh__(self, gui_mn):
        # Remove enemy edit widget
        for w in self.en_edit_frame.winfo_children():
            if w:
                try:
                    self.to_refresh.remove(w)
                except ValueError:
                    pass
                w.destroy()

        # Create enemy edit panel if an enemy is selected
        if gui_mn.selected_enemy_idx != None:

            self.en_edit = Templ_EnemyEdit(self.en_edit_frame)
            self.en_edit.pack(fill=BOTH, expand=TRUE)
            self.to_refresh.append(self.en_edit)

        # Otherwise create enemy list to select an enemy
        else:
            self.en_list = Templ_EnemyList(self.en_edit_frame)
            self.en_list.pack(fill=BOTH, expand=TRUE)
            self.to_refresh.append(self.en_list)

        # Refresh others
        super().__refresh__(gui_mn)

class Templ_SimPlayPanel(SubmenuBase):
    """!
    Panel next to viewport when in SIM_PLAY mode
    """

    def __make__(self, cnf):
        # Create panel name Label
        Label(self,
            text="Sim_Play Mode"
            ).pack(padx=3, pady=10)

        # Create movement buttons

        # Create frame, so we can use .grid
        mv_lframe = LabelFrame(self, text="Movement")
        mv_lframe.pack(fill=X, padx=3, pady=3)
        # Create internal frame for central alignment and padding
        mv_frame = Frame(mv_lframe)
        mv_frame.pack(padx=3, pady=3)

        # Create buttons and add to dictionary
        self.mv_btns = {}
        arrows = "^>v<"
        for i, side in enumerate(TileEdges): # For each tile edge
            self.mv_btns[side] = Button(mv_frame, width=3,
                text=arrows[i], state=DISABLED)
            col, row = side.value
            self.mv_btns[side].grid(row=-row+1, column=col+1)


        # Create button to restart level
        self.restart_btn = Button(self, text="Restart")
        self.restart_btn.pack(padx=3, pady=3)

    def __refresh__(self, gui_mn):


        # Set movement buttons commands

        # Set all buttons to disabled
        self.restart_btn.config(state=DISABLED, command=None)
        for side, btn in self.mv_btns.items():
            btn.config(state=DISABLED, command=None)

        # Set actual move target buttons to enabled
        if gui_mn.can_pl_move:
            # Set restart button command
            self.restart_btn.config(state=NORMAL,
                command=gui_mn.restart_level)
            # Set movement keys command
            pl = gui_mn.game_mn.get_player()
            for side, tile in pl.get_move_targets():
                self.mv_btns[side].config(state=NORMAL,
                    # Tell GameManager to move the player for safety
                    command=lambda s=side: gui_mn.game_mn.move_player(s))


class Templ_EditorModePanel(SubmenuBase):
    """!
    Wrapper to choose appropriate mode panel to show
    """

    def __refresh__(self, gui_mn):
        # Destroy preview panel
        for p in self.winfo_children():
            p.destroy()

        # Make appropriate mode panel

        if gui_mn.editor_mode == EditorModes.VIEW:
            panel = Templ_ViewPanel(self)
        elif gui_mn.editor_mode == EditorModes.EDIT:
            panel = Templ_EditPanel(self)
        elif gui_mn.editor_mode == EditorModes.SIM_PLAY:
            panel = Templ_SimPlayPanel(self)

        # Pack new panel
        panel.pack(fill=BOTH, expand=TRUE)
        panel.__refresh__(gui_mn) # Initial refresh


# LevelEditor Menu Bar (application level)

class AppMenuBar(PopupBase):
    """!
    Application level menu bar for menus like File, Edit, etc
    """
    def __make__(self, cnf):
        # Add file pulldown menu
        file_menu = Menu(self, tearoff=0)
        file_menu.add_command(label="New", command=self.gui_mn.file_new)
        file_menu.add_command(label="Open", command=self.gui_mn.file_open)
        file_menu.add_command(label="Save", command=self.gui_mn.file_save)
        file_menu.add_command(label="Save As", command=self.gui_mn.file_save_as)
        self.add_cascade(label="File", menu=file_menu)

# LevelEditor (GUI)

class LevelEditor_GUI(SubmenuBase):
    """!
    Collection of level editor submenus that make up the entire GUI
    for the level editor. Responsible for calling __refresh__ on submenus
    when GUI needs to react to level change.
    """

    def __make__(self, cnf):
        # Set editor desired size
        self.config(width=700, height=400)
        self.pack_propagate(0) # don't change size to match widgets

        # Create title banner
        self.title_banner = Templ_TitleBanner(self)
        self.title_banner.pack(fill=BOTH, expand=TRUE, padx=10, pady=10)
        self.to_refresh.append(self.title_banner)

        # Create main editor frame
        editor_main_frame = Frame(self)
        editor_main_frame.pack(fill=BOTH, expand=TRUE)

        # Create viewport
        self.viewport = Templ_Viewport(editor_main_frame)
        self.viewport.pack(side=LEFT, fill=Y, padx=10, pady=5)
        self.to_refresh.append(self.viewport)

        # Create editor property panel
        self.editor_prop_panel = Templ_EditorPropPanel(editor_main_frame, width=150)
        self.editor_prop_panel.pack(side=RIGHT, fill=Y, padx=5, pady=5)
        self.editor_prop_panel.pack_propagate(False)
        self.to_refresh.append(self.editor_prop_panel)

        # Create editor mode panel
        self.editor_mode_panel = Templ_EditorModePanel(editor_main_frame, width=150)
        self.editor_mode_panel.pack(side=RIGHT, fill=Y, padx=5, pady=5)
        self.editor_mode_panel.pack_propagate(False)
        self.to_refresh.append(self.editor_mode_panel)


def main():
    # Set a sample submenu class
    sample_menu = Templ_SimPlayPanel

    window = Tk()
    sample_menu(window).pack()
    window.mainloop()

if __name__ == '__main__':
    main()
