"""!
Tile and TileManager classes

TileManager should be used for all tile related function
"""

# Imports
from tilegame_prototype.game_utils.common import Loc, Common
from tilegame_prototype.game_utils.constants import TileEdges


# Tile classes

class Tile(object):
    """!
    A single square tile in the level.
    Visual representation is a box in level editor viewport.
    """

    # Connected tile at each of the sides of the square
    ## Connected tile at top of square tile
    tile_top = None
    ## Connected tile at right of square tile
    tile_right = None
    ## Connected tile at bottom of square tile
    tile_bottom = None
    ## Connected tile at left of square tile
    tile_left = None

    ## Friendly identifier for this tile
    friendly_name = "TILE"

    ## Offset from origin tile, in horizontal and vertical tiles
    origin_offset = Loc(0, 0)

    def __str__(self):
        return self.friendly_name

    def __repr__(self):
        return self.friendly_name

    def get_connected(self, yield_edge=True):
        """!
        Check if tiles for TOP/RIGHT/BOTTOM/LEFT are connected and yield
        with the actual tile object if a connection exists.

        yield: (TileEdges enum, Tile)
            (Tile) if yield_edge is False
        """
        if yield_edge:
            if self.tile_top:
                yield TileEdges.TOP, self.tile_top
            if self.tile_right:
                yield TileEdges.RIGHT, self.tile_right
            if self.tile_bottom:
                yield TileEdges.BOTTOM, self.tile_bottom
            if self.tile_left:
                yield TileEdges.LEFT, self.tile_left
        else:
            if self.tile_top:
                yield self.tile_top
            if self.tile_right:
                yield self.tile_right
            if self.tile_bottom:
                yield self.tile_bottom
            if self.tile_left:
                yield self.tile_left

class TileManager(object):
    """!
    Manager of tiles in the level.
    Allows creation of connected tiles.
    Gives tile offsets for visual representation in viewport.
    """

    # Friendly name suffixes

    ## Suffix for origin tile
    tile_suff_origin = ": ORIGIN"
    ## Suffix for finish tile
    tile_suff_finish = ": FINISH"
    ## Whether to add suffix to tile friendly_name
    add_suffix_to_name = False


    # Tile references

    ## Origin tile reference
    origin_tile = None
    ## Finish tile reference
    finish_tile = None

    def __init__(self, add_suffix_to_name=False):
        """!
        Initialise tile manager with origin tile

        @param add_suffix_to_name: (bool) whether to add origin and finish
            suffixes to tile name
        """

        self.add_suffix_to_name = add_suffix_to_name

        # Initialise tile grid
        ## Grid of tiles hashed by their origin_offset
        ## (grid_position : Tile_object)
        self.tile_grid = {}

        # Create initial tile (player start tile)
        self.origin_tile = Tile()
        self.origin_tile.friendly_name = Common.letter_code_by_idx(0)
        if add_suffix_to_name:
            self.origin_tile.friendly_name += self.tile_suff_origin
        self._add_to_grid(self.origin_tile)

    def reset(self):
        """!
        Reset own values to initial position
        """
        self.origin_tile = None
        self.finish_tile = None
        self.tile_grid = {}

        # Create origin tile
        self.origin_tile = Tile()
        self.origin_tile.friendly_name = Common.letter_code_by_idx(0)
        if self.add_suffix_to_name:
            self.origin_tile.friendly_name += self.tile_suff_origin
        self._add_to_grid(self.origin_tile)

    def set_tiles_from_load(self, tiles_dict):
        """!
        Create new tiles and connections loaded from JSON file.
        """
        # First, create the tiles
        for ori_off, tile_dict in tiles_dict["grid"].items():
            # Parse origin_offset as Loc of integers
            ori_off = Loc(*(int(x) for x in ori_off.strip("()").split(",")))
            # Add tile
            self.add_tile_from_load(ori_off, tile_dict)

        # Then set connections
        for ori_off, tile_dict in tiles_dict["grid"].items():
            # Parse origin_offset as Loc of integers
            ori_off = Loc(*(int(x) for x in ori_off.strip("()").split(",")))
            # Set connections
            self.connect_tile_from_load(ori_off, tile_dict)

        # Finish off by setting additional manager options
        fin_ori_off = tiles_dict["finish_tile"]
        # Parse origin_offset as Loc of integers
        fin_ori_off = Loc(*(int(x) for x in fin_ori_off.strip("()").split(",")))
        self.finish_tile = self.get_tile_at(fin_ori_off)

    def get_tile_coords(self, tile):
        """!
        Return tile's coordinates in this manager's tile_grid for
        a key in the tile_grid.

        @param tile: (Tile object) tile to find coords of

        @return (str) tile grid coords of specified tile, in '(X, Y)' form
        """
        return repr(tile.origin_offset)

    def get_tile_at(self, x, y=None):
        """!
        Return the tile object at the given X and Y coordinates.

        @param x: (int/Loc) x of search coords. Can also contain X and Y
        @param y: (int) y of search coords

        @return (Tile object) the tile at the desired location, or None
            if not found.
        """

        if y is not None:
            position = Loc(x, y)
        else:
            position = Loc(*x)

        try:
            # Return the desired tile if found
            return self.tile_grid[repr(position)]
        except KeyError:
            # Otherwise don't return anything
            return None

    def _add_to_grid(self, tile):
        """!
        Add a tile to the grid using its origin offset. Will not add if a tile
        already exists in the same place.

        @param tile: (Tile object) tile to add to grid

        @return (bool) whether tile was added
        """
        # Get desired coordinates for tile, as a string
        position = self.get_tile_coords(tile)

        # Check if a tile already exists there
        if self.tile_grid.get(position):
            return False

        # Otherwas set the reference
        self.tile_grid[position] = tile
        return True

    def _remove_extraneous(self):
        """!
        Analyse the grid and remove any tile that aren't connected to the origin

        Currently only check if a tile has not connections to remove it
        """
        rogue_locs = [] # Locations where tile has not connections

        for loc, tile in self.tile_grid.items():
            # Check if this tile has any connections
            has_connections = False
            for edge in tile.get_connected():
                has_connections = True
                break

            # Mark for removal
            rogue_locs.append(loc)

        # Remove rogue tile
        if not has_connections:
            for loc in rogue_locs:
                # Always keep the origin tile!
                if self.tile_grid[loc] != self.origin_tile:
                    # Clear reference to finish tile
                    if self.tile_grid[loc] == self.finish_tile:
                        self.finish_tile = None

                    # Remove tile from grid
                    self.tile_grid.pop(loc)

    def _remove_extraneous_from(self, start_loc):
        """!
        Remove tiles connected at start_loc if they aren't connected
        to the origin.

        @param start_loc: (Loc) origin offset to search from
        """
        # Check branches from starting location
        for edge in TileEdges:
            start_tile = self.get_tile_at(start_loc + edge.value)
            if start_tile:
                connected = self.get_all_connected(start_tile)

                # Was origin in this branch
                if self.origin_tile not in connected:
                    # Remove all tiles
                    for tile in connected:
                        self._disconnect_tile_edges(tile)
                        self._remove_from_grid(tile)

    def _connect_tile_edges(self, other_tile, other_edge, new_tile):
        """!
        Connect tiles at an edge. Will set opposite edge for new_tile.

        @param other_tile: (Tile) tile to connect from
        @param other_edge: (TileEdges edge) edge of other_tile to
            connect from
        @param new_tile: (Tile): tile to connect to
        """
        if other_edge == TileEdges.TOP:
            other_tile.tile_top = new_tile
            new_tile.tile_bottom = other_tile
        elif other_edge == TileEdges.RIGHT:
            other_tile.tile_right = new_tile
            new_tile.tile_left = other_tile
        elif other_edge == TileEdges.BOTTOM:
            other_tile.tile_bottom = new_tile
            new_tile.tile_top = other_tile
        elif other_edge == TileEdges.LEFT:
            other_tile.tile_left = new_tile
            new_tile.tile_right = other_tile

    def _disconnect_tile_edges(self, other_tile, other_edge=None):
        """!
        Disconnect tiles at an edge by removing edge references. If other_edge
        is specified, only that edge connection (and reference on connected tile)
        will be removed.

        @param other_tile: (Tile) one of either tile to involve in disbandment
        @param other_edge: (TileEdges enum) edge of specified tile to disconnect from
            If None, all edges will be disconnected
        """

        # Disconnect all edges if required
        if other_edge == None:
            for edge, tile in other_tile.get_connected():
                self._disconnect_tile_edges(other_tile, edge)
            return

        if other_edge == TileEdges.TOP:
            # Store connected tile in temp variable
            new_tile = other_tile.tile_top
            # Ensure there is a connected tile there
            if new_tile:
                # Remove references
                other_tile.tile_top = None
                new_tile.tile_bottom = None

        # Repeat for other edges
        elif other_edge == TileEdges.RIGHT:
            new_tile = other_tile.tile_right
            if new_tile:
                other_tile.tile_right = None
                new_tile.tile_left = None
        elif other_edge == TileEdges.BOTTOM:
            new_tile = other_tile.tile_bottom
            if new_tile:
                other_tile.tile_bottom = None
                new_tile.tile_top = None
        elif other_edge == TileEdges.LEFT:
            new_tile = other_tile.tile_left
            if new_tile:
                other_tile.tile_left = None
                new_tile.tile_right = None

    def _remove_from_grid(self, tile):
        """!
        Remove a tile from the grid (does not check references).
        Use remove_tile for proper tile removal
        """
        self.tile_grid.pop(self.get_tile_coords(tile))

    def add_tile(self, other_tile, other_edge, auto_connect=True):
        """!
        Create a new tile attached to an existing tile. Will not work if a tile
        is already attached at that position.

        @param other_tile: (Tile object) existing tile to attach to
        @param other_edge: (TileEdges edge) edge to attach to
        @param auto_connect: (bool) whether to connect non specified edges such
            as when defining loops. Disable to only use specified connections

        @return (Tile object) tile that was created. Otherwise None if tile
            could not be created
        """

        # Get location of new tile

        # Tile offset to other_tile in form (horizontal, vertical)
        tile_offset = other_edge.value

        # Set tile origin offset (unique location)
        new_loc = other_tile.origin_offset + tile_offset

        if self.tile_grid.get(repr(new_loc)):
            return None # Return if tile already exists in this position


        # Create new tile object

        new_tile = Tile()
        new_tile.origin_offset = new_loc

        # Set tile friendly name as a character
        chr_idx = len(self.tile_grid)
        new_tile.friendly_name = Common.letter_code_by_idx(chr_idx)

        # Connect specified adjoining edges in both tiles
        self._connect_tile_edges(other_tile, other_edge, new_tile)

        # Set reference in tile grid
        self._add_to_grid(new_tile)


        # Connect adjacent tile edges, if any
        if auto_connect:

            # Example tile grid
            #        c N
            #        a b
            # If adding N to b's TOP, also detect and connect N to c's RIGHT

            for edge in TileEdges:
                adj_tile = self.get_tile_at(new_loc + edge.value)
                if adj_tile:
                    self._connect_tile_edges(new_tile, edge, adj_tile)


        # Return created tile
        return new_tile

    def add_tile_from_load(self, origin_offset, tile_dict):
        """!
        Create new tile using data loaded from JSON file. Will also add the
        tile to the grid. Will not replace tile in same grid place.

        @warning The tile will have no connections
        """

        # Create and set attributes
        new_tile = Tile()
        new_tile.origin_offset = origin_offset
        new_tile.friendly_name = tile_dict["friendly_name"]

        # Add to grid
        self._add_to_grid(new_tile)

    def connect_tile_from_load(self, origin_offset, tile_dict):
        """!
        Set connection references for tile_dict loaded from JSON file.
        """

        # Get tile to connect from
        this_tile = self.get_tile_at(origin_offset)
        if not this_tile:
            return

        # Perform connection for each edge
        for edge_name in tile_dict["connections"]:
            edge = TileEdges[edge_name] # Get corresponding enum value

            adj_tile = self.get_tile_at(origin_offset + edge.value)
            if adj_tile:
                self._connect_tile_edges(this_tile, edge, adj_tile)

    def remove_tile(self, tile):
        """!
        Remove a tile from the tile grid

        @param tile: (Tile) tile to remove
        """

        # Don't remove origin tile
        if tile == self.origin_tile:
            return

        # Save location of tile for extraneous removal
        old_loc = tile.origin_offset.copy()

        # Remove all references to tile
        if tile == self.finish_tile:
            self.finish_tile = None # Check finish tile
        for edge, other_tile in tile.get_connected():
            self._disconnect_tile_edges(tile, edge) # Check connected edges

        # Remove tile from grid
        self._remove_from_grid(tile)

        # Remove extraneous tiles
        self._remove_extraneous_from(old_loc)

    def set_finish_tile(self, tile):
        """!
        Set the specified tile to be the player's finish tile

        @param tile: (Tile) tile to make the finish tile. Cannot be
            origin tile.
        """
        # Don't allow origin to be finish
        if tile == self.origin_tile:
            return

        # Reset existing finish tile if any
        if self.finish_tile:
            self.finish_tile.friendly_name = self.finish_tile.friendly_name.replace(self.tile_suff_finish, "")

        # Set new finish tile reference
        self.finish_tile = tile

        # Set friendly name for easy access
        if self.add_suffix_to_name:
            tile.friendly_name += self.tile_suff_finish # eg: 'B: FINISH'

    def get_grid_bounds(self):
        """!
        Return bounding box of grid coords within which all current
        tiles are contained

        @return (2 Loc objects) 2 corner coords of bounds of tile grid
        """
        min_loc = Loc(0, 0)
        max_loc = Loc(0, 0)

        for loc, tile in self.tile_grid.items():
            # Check x component
            if tile.origin_offset.x < min_loc.x:
                min_loc.x = tile.origin_offset.x
            elif tile.origin_offset.x > max_loc.x:
                max_loc.x = tile.origin_offset.x

            # Check y component
            if tile.origin_offset.y < min_loc.y:
                min_loc.y = tile.origin_offset.y
            elif tile.origin_offset.y > max_loc.y:
                max_loc.y = tile.origin_offset.y

        return min_loc, max_loc

    def get_grid_size(self):
        """!
        Return magnitude of tile grid in length and width

        @return (Loc object) size of width and length as X and Y respectively
        """
        min_loc, max_loc = self.get_grid_bounds()

        ret = max_loc - min_loc # find difference between bounding corners
        ret.x = abs(ret.x) + 1 # consider 0, grid always passes through origin
        ret.y = abs(ret.y) + 1

        return ret

    def is_origin(self, tile):
        """!
        Return whether tile is the origin tile
        """
        return tile and tile == self.origin_tile

    def is_finish(self, tile):
        """!
        Return whether tile is the finish tile
        """
        return tile and tile == self.finish_tile

    def get_full_name(self, tile):
        """!
        Return full name of tile. Suffix includes irregardless of
        add_suffix_to_name. Mostly used for debugging or verbose
        display names.
        """
        return "%s%s" % (tile.friendly_name,
               self.tile_suff_origin if self.is_origin(tile) else \
               self.tile_suff_finish if self.is_finish(tile) else "")


    def get_all_connected(self, start_tile, *args):
        """!
        Get a set of all tiles connected directly or indirectly to start_tile

        get_all_connected(start_tile) -> finds tiles connected to start_tile
        """
        # First call
        if start_tile:
            connected = set()
            connected.add(start_tile)
            to_check = [start_tile]
        # Recursive call
        else:
            to_check, connected = args

        # Add proposed tiles connections
        sz = len(connected)
        new_tiles = set()
        for tile in to_check:
            for tile1 in tile.get_connected(False):
                new_tiles.add(tile1)
                connected.add(tile1)

        # Were unique elements added to the set?
        if len(connected) == sz:
            return connected
        else:
            return self.get_all_connected(None, new_tiles, connected)

    def __str__(self):
        ret = ""
        tiles_to_check = [self.origin_tile]
        for to_check in tiles_to_check:
            ret += "%s %s\n" % (self.get_full_name(to_check),
                                to_check.origin_offset)
            for edge, tile in to_check.get_connected():
                if tile not in tiles_to_check:
                    tiles_to_check.append(tile)
                ret += " %s -> %s\n" % (edge, tile)

        return ret
