"""!
Classes and tools for easily creating GUI menus for the
level editor
"""

# Imports
import tkinter as tk
from tkinter.ttk import Button, Entry
from tilegame_prototype.game_utils.common import Loc, MathStat


# Submenu base classes

class MotionInput(object):
    def __init__(self, *args):
        """!initialise attributes. optionally call bind_to_widget with
        specified args if args are not empty"""
        self._isheld = False

        self._delta = (0, 0)
        self._normalised_delta_max = 5

        self._bound_events = {}
        ##self._held_buttons = {}

        # bind to widget if extra args given
        if args:
            self.bind_to_widget(*args)


    @property
    def delta(self):
        return Loc(self._delta)

    def bind_to_widget(self, in_widget, button="1"):
        """!binds relevant inputs to in_widget, optionally using the
        specified button (1=LMB, 2=MMB, 3=RMB)"""
        in_widget.bind("<ButtonPress-%s>"%button, self.inp_press)
        in_widget.bind("<ButtonRelease-%s>"%button, self.inp_release)
        in_widget.bind("<Button%s-Motion>"%button, self.inp_motion)
        # add to held buttons dict (defualt False not held)
        ##self._held_buttons[button] = False

    def bind(self, event_code=None, func=None, add=None):
        """!Binds func to be called on event_code.
        Event codes is written in the format <MODIFIER-MODIFIER-IDENTIFIER>.
        Available MODIFIERS:
            Motion
        Available IDENTIFIERS:
            X
            Y
            XY"""
        event_code = event_code.replace("<", "").replace(">", "")
        keys = event_code.split("-")
        identifier = keys.pop()
        modifiers = keys
        # check if the event_code is valid
        if identifier not in ["X", "Y", "XY"]:
            return False # fail
        for m in modifiers:
            if m not in ["Motion", "Button1", "Button2", "Button3"]:
                return False # epic fail!

        # bind the function
        # create new list for event if not already bound
        if event_code not in self._bound_events:
            self._bound_events[event_code] = [func]
        else:
            # append to list if necessary
            if add:
                self._bound_events[event_code].append(func)
            # otherwise initialise new list
            else:
                self._bound_events[event_code] = [func]
        return True # success

    def _get_bound_events(self, identifier=None, *modifiers):
        """!Returns list of bound functions to call for the specified event"""
        ret_funcs = []
        modifiers = set(modifiers) # ensure modifiers are unique

        # check every bound event code
        for event_code, func_list in self._bound_events.items():
            event_keys = event_code.split("-")
            event_id = event_keys.pop()
            event_mods = event_keys
            all_mods_work = True

            # check identifier
            id_works = (identifier is None
                        or identifier is not None and identifier == event_id)
            # only check modifiers if identifier is correct
            if id_works:
                for m in modifiers:
                    if m not in event_mods:
                        all_mods_work = False
                        break
            # add bound functions if id and modifiers are correct
            if id_works and all_mods_work:
                    ret_funcs = ret_funcs + func_list

        # finally return found functions
        return ret_funcs if ret_funcs else None

    def _normalise_delta(self, in_delta, set_in_place=True):
        """!Normalises in_delta to range (-1, 1).
        Optionally don't set in place"""

        # set attributes of passed in list object if necessary
        if set_in_place:
            in_delta.x = MathStat.map_range(in_delta.x,
                                          -self._normalised_delta_max,
                                          self._normalised_delta_max, -1, 1)
            in_delta.y = MathStat.map_range(in_delta.y,
                                          -self._normalised_delta_max,
                                          self._normalised_delta_max, -1, 1)
            return in_delta

        # otherwise initialise a new Loc object
        else:
            return Loc(MathStat.map_range(in_delta.x,
                                          -self._normalised_delta_max,
                                          self._normalised_delta_max, -1, 1),
                       MathStat.map_range(in_delta.y,
                                          -self._normalised_delta_max,
                                          self._normalised_delta_max, -1, 1))

    def _is_held(self, button):
        """!returns whether the button is held"""
        return button in self._held_buttons and self._held_buttons[button]

    def inp_press(self, event, func=None):
        """!Bind this to a widget on a ButtonPress-X event"""
        self._isheld = True
        ##self._held_buttons[event.num] = True
        self._last_loc = Loc(event.x, event.y)
        self._orig_press_loc = self._last_loc.copy()
        # call function if specified with event
        if func:
            func(event)

    def inp_release(self, event=None, func=None):
        """!Bind this to a widget on a ButtonRelease-X event"""
        self._isheld = False
        ##self._held_buttons[event.num] = False
        # call function if specified with event
        if func:
            func(event)

    def inp_motion(self, event, func=None):
        """!Bind this to a widget on a ButtonX-Motion event"""
        def near_to_zero(val, offset=0.05):
            return val < offset and val > -offset

        if self._isheld:
            # get and set delta
            new_loc = Loc(event.x, event.y)
            self._delta = d = self._normalise_delta(new_loc - self._last_loc)
            # set delta in event object to return in callbacks
            event.delta = d
            # ensure the last location is updated for next motion
            self._last_loc = new_loc

            # call function if specified with event and delta (in event)
            if func:
                func(event)

        # fire bound events for button invariant bindings

        if not near_to_zero(d.x):
            be = self._get_bound_events("X", "Motion")
            if be:
                for func in be:
                    func(event)

        if not near_to_zero(d.y):
            be = self._get_bound_events("Y", "Motion")
            if be:
                for func in be:
                    func(event)

        if not near_to_zero(d.x) or not near_to_zero(d.y):
            be = self._get_bound_events("XY", "Motion")
            if be:
                for func in be:
                    func(event)

class SubmenuBase(tk.Frame):
    """!
    A template submenu frame class for the level editor with
    multiple widgets attached
    """

    ## Submenus to refresh when this submenu gets refreshed
    ## @warning Ensure submenus in to_refresh actually have an override of __refresh__
    ##to_refresh = []

    def __init__(self, master=None, cnf={}, **kw):
        """!
        Create submenu
        """
        # Reset to_refresh so that previous class's version isn't used

        ## Submenus to refresh when this submenu gets refreshed
        ## @warning Ensure submenus in to_refresh actually have an override of __refresh__
        self.to_refresh = []

        # Set options before Frame initialisation
        self.__preconfig__(kw)

        # Initialise frame parent
        tk.Frame.__init__(self, master=master, cnf=cnf, **kw)

        # Create submenu specific widgets
        cnf.update(kw)
        self.__make__(cnf)

    def __preconfig__(self, kw):
        """!
        Child submenus should override this to set initial values
        from init options

        @param kw: (dict) optional params supplied for creation
        """
        pass

    def __make__(self, cnf):
        """!
        Child submenus should override this to make the additional
        widgets they need

        @param cnf: (dict) optional params supplied for creation
        """
        pass

    def __refresh__(self, gui_mn):
        """!
        Called when something changed in the level editor that submenus
        may need to react to. If not overridden, will refresh all submenus
        in to_refresh list.

        @param gui_mn: (GUIManager object) guimanager to sync to and
            configure callbacks to
        """

        # Refresh all attached submenus
        for sub in self.to_refresh:
            if sub:
                sub.__refresh__(gui_mn)
            else:
                self.to_refresh.remove(sub)

class PopupBase(tk.Menu):
    """!
    A template popup menu class for the level editor with
    multiple menu items attached, without tearoff
    """

    def __init__(self, master=None, cnf={}, **kw):
        """!
        Create popup
        """
        # Set options before Frame initialisation
        self.__preconfig__(kw)

        # Initialise frame parent
        tk.Menu.__init__(self, master=master, cnf=cnf, tearoff=False, **kw)

        # Create popup specific items
        cnf.update(kw)
        self.__make__(cnf)

    def __preconfig__(self, kw):
        """!
        Child popups should override this to set initial values
        from init options

        @param kw: (dict) optional params supplied for creation
        """
        # Set useful values for use with menu items commands if specified

        ## Reference to the gui manager
        self.gui_mn = kw.pop("guimanager",
                      kw.pop("g", None))

    def __make__(self, cnf):
        """!
        Child popups should override this to make the additional
        items they need

        @param cnf: (dict) optional params supplied for creation
        """
        pass

class CircularFrame(tk.Frame):
    """!
    Frame with items positioned in a circle
    Useful when each item is small to avoid overlap.
    """

    def arrange(self, padding=0.3):
        """!
        Arrange child widgets in a circle. Doesn't require widgets
        to be packed/grided.

        @param padding: (float) relative gap from frame edge to center (0 to 1)
            0 means closer to edges, 1 means closer to center
        """
        for i, child in enumerate(self.winfo_children()):
            x, y = self.get_widget_pos(i, padding)
            child.place(relx=x, rely=y, anchor=tk.CENTER)


    def get_widget_pos(self, n, padding):
        """!
        Find X and Y coordinates of nth widget

        @param n: (int) widget number to check
        @param padding: (float) distance from frame edge (0 to 0.5)

        @return (X, Y) coords of point
        """

        # Number of circle vertices
        tot_n = len(self.winfo_children())
        if tot_n == 0:
            return

        # Radius of circle
        rad = 0.5 - MathStat.map_range(padding, 0, 1, 0, 0.5)

        return MathStat.getpos_circle(n, tot_n, rad, (0.5, 0.5))

class EditableText(SubmenuBase):
    """!
    Entry widget with callback for when text is changed
    """

    ## Frame shown when editing text
    _change_frame = None
    ## Last valid text
    last_saved = ""
    ## Whether the hint text is currently showing
    _is_hinting = True
    ## Timer handle for scheduled Key event reaction
    _key_timer = None

    def __preconfig__(self, kw):
        self._callback = kw.pop("command", None)
        self._hint = kw.pop("hint", "")
        self._ent_var = kw.pop("textvariable", None)

        # Save value if textvariable given
        if self._ent_var:
            self.last_saved = self._ent_var.get()

        # Otherwise, create a new stringholder
        else:
            self._ent_var = tk.StringVar()

    def __make__(self, cnf):
        ent = Entry(self, textvariable=self._ent_var)
        ent.bind("<FocusIn>", self.__on_focus_in)
        ent.bind("<FocusOut>", self.__on_focus_out)
        ent.bind("<Key>", self.__on_key)
        ent.bind("<ButtonPress-1>", self.__on_focus_in)
        ent.pack(side=tk.LEFT)

        self._ent_var.set(self._hint)
        self.__hint_check()

    def __on_save(self):
        new = self._ent_var.get()

        if self._is_hinting or new == self._hint:
            self.last_saved = new = ""
        else:
            self.last_saved = new

        if self._callback:
            self._callback(new)
        self.leave_edit()

    def __on_cancel(self):
        self._ent_var.set(self.last_saved)
        self.__on_save()

    def __on_focus_in(self, event):
        if self._change_frame:
            return

        self._change_frame = tk.Frame(self)
        self._change_frame.pack(side=tk.LEFT)

        sv_btn = Button(self._change_frame,
            text="OK", command=self.__on_save)
        sv_btn.bind("<FocusOut>", self.__on_focus_out)
        sv_btn.pack(side=tk.LEFT)

        cnl_btn = Button(self._change_frame,
            text="Cancel", command=self.__on_cancel)
        cnl_btn.bind("<FocusOut>", self.__on_focus_out)
        cnl_btn.pack(side=tk.LEFT)

        self.__clear_hint()

    def __on_focus_out(self, event):
        if self.focus_get() not in self._change_frame.winfo_children():
            self.__on_cancel()

    def __on_key(self, event):
        self.__on_focus_in(None)

        try:
            if event.keysym not in ("BackSpace", "Delete"):
                char = event.char
            else:
                char = None
        except AttributeError:
            char = None

        if not self._key_timer:
            self._key_timer = self.after(30, lambda: self.__hint_check(char))

    def leave_edit(self):
        """!
        Destroy save and cancel buttons and refresh hint
        """
        self._ent_var.set(self.last_saved)

        if self._change_frame:
            self._change_frame.destroy()
        self._change_frame = None

        if not self.last_saved:
            self.__show_hint()
        self.__hint_check()

    def __show_hint(self):
        if not self._ent_var.get():
            self._ent_var.set(self._hint)

    def __clear_hint(self):
        if self._is_hinting or self._ent_var.get() == self._hint:
            self._ent_var.set("")
            self._is_hinting = False

    def __hint_check(self, char=None):
        self._key_timer = None

        if self._hint:
            if self._is_hinting and self._ent_var.get() != self._hint \
            or char and self._ent_var.get() == self._hint:
                self._ent_var.set(char if char else "")
                self._is_hinting = False
                self.__on_focus_in(None)
            elif not self._is_hinting and not self._ent_var.get():
                self._ent_var.set(self._hint)
                self._is_hinting = True
            else:
                self._is_hinting = False
