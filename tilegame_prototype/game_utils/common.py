"""!
Contains classes that many parts of the game may use
"""

# Imports

from random import random
from math import sqrt, sin, cos, pi
from tilegame_prototype.game_utils.constants import TileEdges
from string import ascii_uppercase


# Common classes

class Loc(list):
    """!
    Vector with built in vector and scalar arithmetic support
    """

    _repr_items = "XYZ"

    def get_named(self, i):
        return self[i]
    def set_named(self, i, value):
        self[i] = value

    # allow index 0 - 2 to be accessed through letters:
    # x,y,z or r,g,b. IndexError if list too small!
    x = r = property(lambda self: self.get_named(0),
                     lambda self, v: self.set_named(0,v))
    y = g = property(lambda self: self.get_named(1),
                     lambda self, v: self.set_named(1,v))
    z = b = property(lambda self: self.get_named(2),
                     lambda self, v: self.set_named(2,v))

    def set(self, *args):
        self.__init__(*list(args))

    def __init__(self, *args):
        super().__init__(list(args))

    def __repr__(self):
        return "(%s)"%", ".join((str(it) for it in self))
    def __str__(self):
        return "(%s)"%", ".join(("%s=%s"%(self._repr_items[i], str(it))
                                 if i < len(self._repr_items) else str(it)
                                 for i, it in enumerate(self)))
    def __add__(self, other):
        ret = Loc()
        for i in range(len(self)):
            try:
                ret.append(self[i] + other[i])
            except TypeError:
                ret.append(self[i] + other)
        return ret
    def __mul__(self, other):
        ret = Loc()
        for i in range(len(self)):
            try:
                ret.append(self[i] * other[i])
            except TypeError:
                ret.append(self[i] * other)
        return ret
    def __sub__(self, other):
        ret = Loc()
        for i in range(len(self)):
            try:
                ret.append(self[i] - other[i])
            except TypeError:
                ret.append(self[i] - other)
        return ret
    def __truediv__(self, other):
        ret = Loc()
        for i in range(len(self)):
            try:
                ret.append(self[i] / other[i])
            except TypeError:
                ret.append(self[i] / other)
        return ret
    def __pow__(self, other):
        ret = Loc()
        for i in range(len(self)):
            try:
                ret.append(pow(self[i], other[i]))
            except TypeError:
                ret.append(pow(self[i], other))
        return ret
    def __round__(self, *args):
        ret = Loc()
        for i in range(len(self)):
            ret.append(round(self[i], *args))
        return ret
    def __iadd__(self, other):
        self = self + other
        return self
    def __isub__(self, other):
        self = self - other
        return self
    def __imul__(self, other):
        self = self * other
        return self
    def __itruediv__(self, other):
        self = self / other
        return self
    def copy(self):
        return Loc(*self)

class MathStat(object):

    @staticmethod
    def clamp(val, min=0, max=1):
        return min if val < min else max if val > max else val

    @staticmethod
    def getpercent(val, min, max):
        """!returns what percent (0 to 1) val is between min and max.
        eg: val of 15 from 10 to 20 will return 0.5"""
        return MathStat.clamp((val-min) / (max-min))

    @staticmethod
    def map_range(val, in_a, in_b, out_a=0, out_b=1):
        """!returns val mapped from range(in_a to in_b) to range(out_a to out_b)
        eg: 15 mapped from 10,20 to 1,100 returns 50"""
        return MathStat.lerp(out_a, out_b, MathStat.getpercent(val, in_a, in_b))

    @staticmethod
    def map_range_clamped(val, in_a, in_b, out_a=0, out_b=1):
        """!returns val mapped from range(in_a to in_b) to range(out_a to out_b)
        eg: 15 mapped from 10,20 to 1,100 returns 50"""
        return MathStat.lerp(out_a, out_b, MathStat.getpercent(val, in_a, in_b))

    @staticmethod
    def lerp(a, b, bias, clamp=True):
        """!returns interpolation between a and b. bias 0 = a, bias 1 = b.
        also works with iterables by lerping each element of a and b
        can also extrapolate if clamp is set to False"""
        def lerp1(a, b, bias):
            return a + (b-a) * bias
        def cross_iter(a, b):
            for i in range(len(a)):
                yield a[i], b[i]
        def cross_iter_str(a, b):
            # format: '#rrggbb...' hex codes
            # yields integers rr, gg, bb, etc for a and b
            for i in range(1, len(a)):
                if i % 2 == 1:
                    yield int("0x%s"%a[i:i+2], 0), int("0x%s"%b[i:i+2], 0)
        if clamp:
            bias = bias if bias > 0 else 1 if bias > 1 else 0
        try:
            # lerp each element in iterable container
            cross_lerp = [lerp1(ax, bx, bias) for ax, bx in cross_iter(a, b)]
            try:
                return type(a)(*cross_lerp)
            except:
                return type(a)(cross_lerp)
        except:
            # string hex code color lerp
            if isinstance(a, str):
                ret_str = "#"
                for ax, bx in cross_iter_str(a, b):
                    ret_str += "%02x"%int(lerp1(ax, bx, bias))
                return ret_str
            # simple lerp
            return lerp1(a, b, bias)

    @staticmethod
    def getdistsquared(loc1, loc2):
        """!returns squared distance between coords"""
        return sum((loc2-loc1)**2)

    @staticmethod
    def getdist(loc1, loc2):
        """!returns distance between coords"""
        return sqrt(sum((loc2-loc1)**2))

    @staticmethod
    def dot(loc1, loc2):
        """!returns dot product of two Loc objects
        The dot product is a multiplication of two vectors that
        results in a scalar
        dot(a, b) = a.x*b.x + a.y*b.y ..."""
        return sum(loc1*loc2)

    @staticmethod
    def getedgelengths(verts):
        return [getdist(coords[0], coords[1],
                        *verts[i+1 if i+1<len(verts) else 0])
                for i, coords in enumerate(verts)]

    @staticmethod
    def getlengthpos(verts, bias):
        """!returns location at an edge between set of verts at the given bias (0-1)"""
        bias = 1 if bias > 1 else 0 if bias < 0 else bias
        edge_lengths = getedgelengths(verts)
        perimeter = sum(edge_lengths)
        desired_point = perimeter * bias

        cursum = 0
        for i, leng in enumerate(edge_lengths):
            cursum += leng
            if cursum >= desired_point:
                vert_hi = verts[i]
                vert_lo = verts[i-1 if i>0 else len(verts)-1]
                break

        return lerp(vert_lo, vert_hi, getpercent(desired_point, cursum-leng,
                    cursum))

    @staticmethod
    def get_random_location_in_bounding_box(origin, bounds):
        return origin + [MathStat.lerp(0, i, random()) for i in bounds]

    @staticmethod
    def getpos_circle(n, tot_n, radius, center):
        """!
        Find X and Y coordinates for a given point in circle

        @param n: (int) circle point to check
        @param tot_n: (int) total number of circle points
        @param radius: (float) radius of circle
        @param center: (Loc) coordinates of center

        @return (X, Y) coords of point
        """

        # Center of circle
        cx, cy = center

        # Calculate angle of vertex in relation to first vertex
        O = (2*pi) / tot_n * n + (pi / tot_n)

        # Calculate and return coordinate of point (X, Y)
        return cx + radius * sin(O), cy + radius * cos(O)


class Common(object):
    """!
    Miscellaneous small tasks
    """
    @staticmethod
    def letter_code_by_idx(idx):
        """!
        Returns alphabetical letter code given an index.
        Eg, 0->A, 1->B ... 26->AA, 27->AB
        """
        # Single character Eg, A, B, C
        if idx < 26:
            return ascii_uppercase[idx]
        # Two characters Eg, AA, AB, AC
        else:
            return ascii_uppercase[idx // 26 - 1] + ascii_uppercase[idx % 26]

    @staticmethod
    def get_reverse_edge(edge):
        """!
        Return an opposite edge for a given TileEdge
        Eg, when given TOP edge, will return BOTTOM
        """
        if edge == TileEdges.TOP:
            return TileEdges.BOTTOM
        elif edge == TileEdges.RIGHT:
            return TileEdges.LEFT
        elif edge == TileEdges.BOTTOM:
            return TileEdges.TOP
        elif edge == TileEdges.LEFT:
            return TileEdges.RIGHT
