"""!
Tile Game constants for use in the program
"""

# Imports
from enum import Enum


# Enums

class TileEdges(Enum):
    """!
    Tile edges for square tiles
    """
    TOP =    (0,  1)
    RIGHT =  (1,  0)
    BOTTOM = (0, -1)
    LEFT =   (-1, 0)

class EditorModes(Enum):
    """!
    Modes for using editor
    """
    VIEW = 0
    EDIT = 1
    SIM_PLAY = 2


# Data structures

class Team(object):
    """!
    Structure of players/bots in the game on the same side
    """
    ## Name of team
    name = ""
    ## List of players/enemies
    pawns = []

    def __init__(self, team_name):
        """!
        Create team with a team name

        @param team_name: (string) name of team
        """
        self.name = team_name
        self.pawns = [] # List of players/enemies

    def add_pawn(self, pawn):
        self.pawns.append(pawn)