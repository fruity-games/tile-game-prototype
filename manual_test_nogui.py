"""
Manual testing configuration for Tile Game Prototype application without GUI
"""

def main():
    from tilegame_prototype.level_editor import LevelEditor
    from tilegame_prototype.game_utils.constants import TileEdges
    from tilegame_prototype.gameplay.pawns import Enemy

    # Create level editor in window
    lvl_edit = LevelEditor()
    lvl_edit.level_name = "Demo Level"


    # Comment out below lines to create and save the level

    # Add demo tiles
    a = lvl_edit.tile_mn.origin_tile
    b = lvl_edit.tile_mn.add_tile(a, TileEdges.TOP)
    c = lvl_edit.tile_mn.add_tile(a, TileEdges.RIGHT)
    d = lvl_edit.tile_mn.add_tile(a, TileEdges.LEFT)
    e = lvl_edit.tile_mn.add_tile(b, TileEdges.LEFT)
    f = lvl_edit.tile_mn.add_tile(e, TileEdges.LEFT)
    g = lvl_edit.tile_mn.add_tile(f, TileEdges.LEFT)
    h = lvl_edit.tile_mn.add_tile(g, TileEdges.TOP)
    lvl_edit.tile_mn.remove_tile(g)
    lvl_edit.tile_mn.set_finish_tile(f)

    # Add demo enemies
    new_enem = Enemy(None, (-2, 1))
    new_enem.add_route_point(TileEdges.RIGHT, 2)
    lvl_edit.enemies_setup.append(new_enem)

    new_enem = Enemy(None, (1, 0))
    new_enem.add_route_point(TileEdges.LEFT, 2)
    lvl_edit.enemies_setup.append(new_enem)

##    # Essentials to save level
##    lvl_edit.clear_current()
##    lvl_edit.level_name = "Demo Level" # Not essential, but default is DefaultLevel
##    a = lvl_edit.tile_mn.origin_tile
##    b = lvl_edit.tile_mn.add_tile(a, TileEdges.TOP)
##    lvl_edit.tile_mn.set_finish_tile(b)

    # Save level to file
    lvl_edit.save_to_file() # Saves to "Demo Level.json"


    # Uncomment below lines to load level

    lvl_edit.load_from_file("Demo Level.json")


if __name__ == '__main__':
    main()

