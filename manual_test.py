"""
Manual testing configuration for Tile Game Prototype application
"""

def main():
    from tkinter import Tk
    from tilegame_prototype.level_editor import LevelEditor
    from tilegame_prototype.game_utils.constants import TileEdges
    from tilegame_prototype.gameplay.pawns import Enemy

    # Create a window for GUI
    window = Tk()
    window.title("Tile Game Prototype - Level Editor")

    # Create level editor in window
    lvl_edit = LevelEditor(window)
    lvl_edit.level_name = "Demo Level"

    # Add demo tiles
    a = lvl_edit.tile_mn.origin_tile
    b = lvl_edit.tile_mn.add_tile(a, TileEdges.TOP)
    c = lvl_edit.tile_mn.add_tile(a, TileEdges.RIGHT)
    d = lvl_edit.tile_mn.add_tile(a, TileEdges.LEFT)
    e = lvl_edit.tile_mn.add_tile(b, TileEdges.LEFT)
    f  = lvl_edit.tile_mn.add_tile(e, TileEdges.LEFT)
    g = lvl_edit.tile_mn.add_tile(f, TileEdges.LEFT)
    h = lvl_edit.tile_mn.add_tile(g, TileEdges.TOP)
    lvl_edit.tile_mn.remove_tile(g)
    lvl_edit.tile_mn.set_finish_tile(f)

    # Add demo enemies
    new_enem = Enemy(None, (-2, 1))
    new_enem.add_route_point(TileEdges.RIGHT, 2)
    lvl_edit.enemies_setup.append(new_enem)

    new_enem = Enemy(None, (1, 0))
    new_enem.add_route_point(TileEdges.LEFT, 2)
    lvl_edit.enemies_setup.append(new_enem)

    lvl_edit.update_gui() # call manually for now

    # Start mainloop
    window.mainloop()

if __name__ == '__main__':
    main()

