# Tile Game Prototype


## Game Concept
A tile based puzzle game similar to Hitman GO. The player must get from a starting
tile to an end tile in the least number of moves. Enemies take prescripted routes
and attack the player if he comes too close. Levels will be premade using a basic
level editor.

## Prototype
The prototype will be 2D for faster development. All assets, including art, UI,
etc are examples and may be changed for the actual game.

There will only be one screen in the prototype: the basic level editor. This will
include Edit mode for editing the loaded level, View mode for navigating the level
and Simulate Play mode for testing movement functionality in the level.

Necessary parts for the prototype:
- tiles that connect to other tiles in a level (node like)
- tile based movement for player and enemy
- different editor modes (Edit, View, Simulate Play)
- basic tile based level editor
- basic enemy route editor
- save and load levels to file

## Language
The prototype will be made using Python 3.7.2 for faster iteration time.
IDEs for use with the project are PyScripter or Visual Studio with Python tools.
PyScripter is recommended due to better stability.

## Documentation
Documentation is available here:
https://fruity-games.gitlab.io/tile-game-prototype-docs