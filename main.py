"""
Entry point for Tile Game Prototype application
"""

from tkinter import Tk
from tilegame_prototype.level_editor import LevelEditor

def main():

    # Create a window for GUI
    window = Tk()
    window.title("Tile Game Prototype - Level Editor")

    # Create level editor in window
    lvl_edit = LevelEditor(window)

    # Start mainloop
    window.mainloop()

if __name__ == '__main__':
    main()

